import tkinter as tk

mot = "PYTHON"
t = [mot[0]] + ["_"] * (len(mot)-1)
n = 0
lettres = []

def marque(l,mot,t):
    for i in range(len(mot)):
        if mot[i] == l:
            t[i] = l
            
def gagne(t):
    for e in t:
        if e == "_":
            return False
    return True

fenetre = tk.Tk()
fenetre.title("Le jeu du pendu !")
fenetre.geometry("400x200")

l = tk.Label(fenetre, text = "Propose une lettre:", font=("arial",12))
l.pack()

AfficheMot=tk.Label(fenetre,text=t,fg="#FFFFFF", bg="#3E3F3F",width=17,font="Helvetica 30 bold italic")
AfficheMot.pack()

e = tk.Entry(fenetre)
e.focus()
e.pack()

def b_clic():
    global lettres,n
    n += 1
    l = e.get().upper()
    marque(l,mot,t)
    lettres.append(l)
    AfficheMot.config(text=t,fg='white')
    e.delete(0,1)
    e.focus()

    if gagne(t) or n == 8:
        b.config(text="Le mot était "+mot)

b = tk.Button(fenetre, text = "Essayer", width=17, command=b_clic)
b.pack()

fenetre.mainloop()

#while True:
#    print(t)
#    l = input("LETTRE ? ").upper()
#    n += 1
#    marque(l,mot,t)
#    lettres.append(l)
#    print(lettres)
#    if gagne(t) or n == 8:
#        break
#print(t)
#print("Le mot était ", mot)